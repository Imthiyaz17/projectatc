package com.atc.qa;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.TakesScreenshot;

public class ATCAutomation {

	public static void main(String[] args) throws Exception {
		Script();
	}
	
	public static void Script() throws Exception {
		
		String chromeDriverPath = ".\\Driver\\ChromeDriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.get("http://automationpractice.com/index.php");
		
		WebElement loginBtn = driver.findElement(By.xpath("//a[@class='login']"));
		loginBtn.click();
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='email']"));
		userName.sendKeys("imthiyaz17@gmail.com");
		
		WebElement passWord = driver.findElement(By.xpath("//input[@id='passwd']"));
		passWord.sendKeys("$hameem17");
		
		WebElement signIn = driver.findElement(By.xpath("//button[@id='SubmitLogin']"));
		signIn.click();
		
		WebElement myAddress = driver.findElement(By.xpath("//span[text()='My addresses']"));
		myAddress.click();
		
		WebElement addAddress = driver.findElement(By.xpath("//a[@title='Add an address']"));
		addAddress.click();
		
		WebElement firstName = driver.findElement(By.xpath("//input[@name='firstname']"));
		firstName.clear();
		firstName.sendKeys("Shameem");
		
		WebElement lastName = driver.findElement(By.xpath("//input[@name='lastname']"));
		lastName.clear();
		lastName.sendKeys("Imthiyaz");
		
		WebElement company = driver.findElement(By.xpath("//input[@name='company']"));
		company.clear();
		company.sendKeys("ATC");
		
		WebElement address1 = driver.findElement(By.xpath("//input[@name='address1']"));
		address1.clear();
		address1.sendKeys("No 27, GST Street");
		
		WebElement address2 = driver.findElement(By.xpath("//input[@name='address2']"));
		address2.clear();
		address2.sendKeys("Near Woodlands Mall");
		
		WebElement city = driver.findElement(By.xpath("//input[@name='city']"));
		city.clear();
		city.sendKeys("Houston");
		
		WebElement state = driver.findElement(By.xpath("//select[@name='id_state']"));
		Select select = new Select(state);
		select.selectByVisibleText("Texas");
		
		WebElement postalCode = driver.findElement(By.xpath("//input[@name='postcode']"));
		postalCode.clear();
		postalCode.sendKeys("75002");
		
		WebElement country = driver.findElement(By.xpath("//select[@name='id_country']"));
		Select select1 = new Select(country);
		select1.selectByVisibleText("United States");
		
		WebElement phone = driver.findElement(By.xpath("//input[@name='phone']"));
		phone.clear();
		phone.sendKeys("7134557442");
		
		WebElement phoneMobile = driver.findElement(By.xpath("//input[@name='phone_mobile']"));
		phoneMobile.clear();
		phoneMobile.sendKeys("9788567866");
		
		WebElement additionalInfo = driver.findElement(By.xpath("//textarea[@name='other']"));
		additionalInfo.clear();
		additionalInfo.sendKeys("Address Information");
		
		WebElement alias = driver.findElement(By.xpath("//input[@name='alias']"));
		alias.clear();
		alias.sendKeys("Office Address");
		
		WebElement submitAddress = driver.findElement(By.xpath("//button[@name='submitAddress']"));
		submitAddress.click();
		
		for (int i=1; i<=3; i++) {
		
		WebElement women = driver.findElement(By.xpath("//a[@title='Women']"));
		Actions action = new Actions(driver);
		action.moveToElement(women).build().perform();
		
		WebElement summerDresses = driver.findElement(By.xpath("//a[@title='Summer Dresses']"));
		summerDresses.click();
		
		WebElement iconList = driver.findElement(By.xpath("//i[@class='icon-th-list']"));
		iconList.click();
		
		WebElement dress = driver.findElement(By.xpath("(//div[@class='product-image-container']//img[contains(@title,'Printed')])["+i+"]"));
		action.moveToElement(dress).build().perform();
		
		WebElement quickView = driver.findElement(By.xpath("(//a[@class='quick-view'])["+i+"]"));
		quickView.click();
		
		WebElement frame = driver.findElement(By.xpath("//iframe[contains(@id,'fancybox')]"));
		driver.switchTo().frame(frame);
		
		WebElement qty = driver.findElement(By.xpath("//input[@name='qty']"));
		qty.clear();
		qty.sendKeys("5");
		
		WebElement size = driver.findElement(By.xpath("//select[@name='group_1']"));
		Select select3 = new Select(size);
		select3.selectByVisibleText("L");
		
		WebElement colour = driver.findElement(By.xpath("//a[@name='Yellow']"));
		colour.click();
		
		WebElement addCart = driver.findElement(By.xpath("//button[@name='Submit']"));
		addCart.click();
		
		driver.switchTo().parentFrame();
		
		WebElement continueShop = driver.findElement(By.xpath("//span[@title='Continue shopping']"));
		continueShop.click();
		
		}
		
		WebElement cart = driver.findElement(By.xpath("//a[@title='View my shopping cart']"));
		cart.click();
		
		WebElement checkout1 = driver.findElement(By.xpath("//span[text()='Proceed to checkout']"));
		checkout1.click();
		
		WebElement checkout2 = driver.findElement(By.xpath("//span[text()='Proceed to checkout']"));
		checkout2.click();
		
		WebElement checkBox = driver.findElement(By.xpath("//input[@id='cgv']"));
		checkBox.click();
		
		WebElement checkout3 = driver.findElement(By.xpath("//button[@name='processCarrier']"));
		checkout3.click();
		
		WebElement payment = driver.findElement(By.xpath("//a[@class='cheque']"));
		payment.click();
		
		WebElement confirmOrder = driver.findElement(By.xpath("//span[text()='I confirm my order']"));
		confirmOrder.click();
		
		WebElement account = driver.findElement(By.xpath("//a[@class='account']"));
		account.click();
		
		WebElement orderDetails = driver.findElement(By.xpath("//span[text()='Order history and details']"));
		orderDetails.click();
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollTo(0,200)");
		
		TakesScreenshot takeSS = ((TakesScreenshot)driver);
		File srcFile = takeSS.getScreenshotAs(OutputType.FILE);
		File destFile = new File(".//Target//Screenshots//"+"Screenshot.png");
		
		FileUtils.copyFile(srcFile, destFile);
		
		driver.quit();
		
	}

}
